﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorSites.Domain
{
   public class Member
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Specialist Specialist { get; set; }
        public int? SpecialistId { get; set; }
        public Clinic Clinic { get; set; }
        public int? ClinicId { get; set; }
        public int? Age { get; set; }
        public string MobNo { get; set; }
        public string Email { get; set; }
        public Practice Practice { get; set; }
        public int? PracticeId { get; set; }
        public Gender Gender { get; set; }
        public int? GenderId { get; set; }
        public string Experience { get; set; }
        public Profile Profile { get; set; }
        public  int? ProfileId { get; set; }
        public DateTime? Date { get; set; }
        public string Comment { get; set; }
    }
    
    public class MemberVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Specialist { get; set; }
        public string Clinic { get; set; }
        public string MobNo { get; set; }
        public string Profile { get; set; }
        public DateTime? Date { get; set; }
    }

    public class MemberAVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? SpecialistId { get; set; }
        public int? ClinicId { get; set; }
        public int? Age { get; set; }
        public string MobNo { get; set; }
        public string Email { get; set; }
        public int? PracticeId { get; set; }
        public int? GenderId { get; set; }
        public string Experience { get; set; }
        public int? ProfileId { get; set; }
        public DateTime? Date { get; set; }
        public string Comment { get; set; }
    }

    public class MemberDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Specialist { get; set; }
        public string Clinic { get; set; }
        public int? Age { get; set; }
        public string MobNo { get; set; }
        public string Email { get; set; }
        public string Practice { get; set; }
        public string Gender { get; set; }
        public string Experience { get; set; }
        public string Profile { get; set; }
        public DateTime? Date { get; set; }
        public string Comment { get; set; }
    }
}
