﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorSites.Domain
{
    public  class Practice
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Member> Members { get; set; }
    }
    public class PracticeVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
