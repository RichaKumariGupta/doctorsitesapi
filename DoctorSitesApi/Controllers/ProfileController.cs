﻿using DoctorSites.DAL;
using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoctorSitesApi.Controllers
{
    public class ProfileController : ApiController
    {
        public bool Post([FromBody]Profile profile)
        {
            using (var context = new DoctorDbContext())
            {
                context.Profiles.Add(profile);
                context.SaveChanges();
            }
            return true;
        }

        public List<ProfileVM> Get()
        {
            List<ProfileVM> profiles = new List<ProfileVM>();
            using (var context = new DoctorDbContext())
            {
                profiles = context.Profiles.Select(g => new ProfileVM { Id = g.Id, Name = g.Name }).ToList();

            }
            return profiles;
        }
    }
}
