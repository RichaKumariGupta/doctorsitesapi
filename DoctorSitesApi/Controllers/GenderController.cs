﻿using DoctorSites.DAL;
using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoctorSitesApi.Controllers
{
    public class GenderController : ApiController
    {
        public bool Post([FromBody]Gender gender)
        {
            using (var context = new DoctorDbContext())
            {
                context.Genders.Add(gender);
                context.SaveChanges();
            }
            return true;
        }

        public List<GenderVM> Get()
        {
            List<GenderVM> genders = new List<GenderVM>();
            using(var context= new DoctorDbContext())
            {
                genders = context.Genders.Select(g => new GenderVM{Id=g.Id,Name= g.Name }).ToList();

            }
            return genders;
        }
    }
}
