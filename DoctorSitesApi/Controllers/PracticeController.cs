﻿using DoctorSites.DAL;
using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoctorSitesApi.Controllers
{

    public class PracticeController : ApiController
    {
        public bool Post([FromBody]Practice practice)
        {
            using(var context= new DoctorDbContext())
            {
                context.Practices.Add(practice);
                context.SaveChanges();
            }
            return true;
        }

        public List<PracticeVM> Get()
        {
            List<PracticeVM> practices = new List<PracticeVM>();
            using (var context = new DoctorDbContext())
            {
                practices = context.Practices.Select(g => new PracticeVM { Id = g.Id, Name = g.Name }).ToList();

            }
            return practices;
        }
    }
}
