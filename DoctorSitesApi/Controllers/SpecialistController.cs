﻿using DoctorSites.DAL;
using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoctorSitesApi.Controllers
{
    public class SpecialistController : ApiController
    {
        public bool Post([FromBody]Specialist specialist)
        {
            using (var context = new DoctorDbContext())
            {
                context.Specialists.Add(specialist);
                context.SaveChanges();
            }
            return true;
        }

        public List<SpecialistVM> Get()
        {
            List<SpecialistVM> specialist = new List<SpecialistVM>();
            using (var context = new DoctorDbContext())
            {
                specialist = context.Specialists.Select(g => new SpecialistVM { Id = g.Id, Name = g.Name }).ToList();

            }
            return specialist;
        }
    }
}
