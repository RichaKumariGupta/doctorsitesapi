﻿using DoctorSites.DAL;
using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoctorSitesApi.Controllers
{
    public class SearchModel
    {
        public List<int> SpecialistIds { get; set; }
        public List<int> ClinicIds { get; set; }
        public List<int> PracticeIds { get; set; }
        public int? GenderId { get; set; }
        public string Experience { get; set; }
    }

    public class MemberController : ApiController
    {
        [Authorize]
        public MemberAVM Get()
        {
            var email = RequestContext.Principal.Identity.Name;
            MemberAVM member = new MemberAVM();
            using(var context=new DoctorDbContext())
            {
                member = context.Members.Where(m => m.Email == email).Select(m1 => new MemberAVM {Id=m1.Id,Name=m1.Name,SpecialistId=m1.Specialist.Id,ClinicId=m1.Clinic.Id,GenderId=m1.Gender.Id,PracticeId=m1.Practice.Id,ProfileId=m1.Profile.Id,MobNo=m1.MobNo,Email= m1.Email,Date=m1.Date,Comment=m1.Comment,Experience=m1.Experience,Age=m1.Age }).First();
            }
            return member;
        }

        public bool put(int id,[FromBody]MemberAVM member)
        {
            using (var context = new DoctorDbContext())
            {
                var m1 = context.Members.First(m => m.Id == id);
                m1.Name = member.Name;
                m1.Email = member.Email;
                m1.MobNo = member.MobNo;
                m1.GenderId = member.GenderId.Value;
                m1.Experience = member.Experience;
                m1.ClinicId = member.ClinicId.Value;
                m1.PracticeId = member.PracticeId.Value;
                m1.ProfileId = member.ProfileId.Value;
                m1.GenderId = member.GenderId.Value;
                m1.SpecialistId = member.SpecialistId.Value;
                m1.Age = member.Age;
                m1.Comment = member.Comment;
                m1.Date = member.Date;
                context.SaveChanges();
            }
            return true;
        }
        public List<MemberVM> post([FromBody]SearchModel model)
        {
            List<MemberVM> members = new List<MemberVM>();
            var email = RequestContext.Principal.Identity.Name;
            using(var context= new DoctorDbContext())
            {
                var member = context.Members.Where(m => m.Email == email).First();
                var profileId = member.ProfileId;
                var searchProfileId = profileId == 1 ? 2 : 1;
                Expression<Func<Member, bool>> predicate = m => m.Profile.Id == searchProfileId;
               if(model.SpecialistIds.Count > 0)
                {
                    predicate = predicate.And(m => model.SpecialistIds.Contains(m.Specialist.Id));
                }
                if (model.PracticeIds.Count > 0)
                {
                    predicate = predicate.And(m => model.PracticeIds.Contains(m.Practice.Id));
                }
                if (model.GenderId !=null)
                {
                    predicate = predicate.And(m =>m.Gender.Id== model.GenderId);
                }
                if (model.ClinicIds.Count > 0)
                {
                    predicate = predicate.And(m => model.ClinicIds.Contains(m.Clinic.Id));
                }
                if (model.Experience != null)
                {
                    predicate = predicate.And(m => m.Experience == model.Experience);
                }
                members = context.Members.Include("Specialists").Include("Profiles").Include("Clinics").Include("Genders").Where(predicate).Select(m => new MemberVM {Id=m.Id, Name = m.Name, Specialist=m.Specialist.Name,Clinic=m.Clinic.Name,Profile=m.Profile.Name,MobNo=m.MobNo,Date=m.Date.Value}).ToList();
                return members;
            }
        }

        public MemberDetail Get(int id)
        {
            MemberDetail detail = new MemberDetail();
            using(var context= new DoctorDbContext())
            {
                detail = context.Members.Include("Practice").Include("Specialists").Include("Profiles").Include("Clinics").Include("Genders").Where(m => m.Id == id).Select(m1 => new MemberDetail { Name = m1.Name, Email = m1.Email, MobNo = m1.MobNo,Experience=m1.Experience,Date=m1.Date,Comment=m1.Comment,Age=m1.Age,Gender=m1.Gender.Name,Practice=m1.Practice.Name,Profile=m1.Profile.Name,Specialist=m1.Specialist.Name,Clinic=m1.Clinic.Name }).First(); 
            }
            return detail;
        }

        public bool Delete(int id)
        {
            using(var context= new DoctorDbContext())
            {
                var member = context.Members.First(m => m.Id == id);
                context.Members.Remove(member);
                context.SaveChanges();
            }
            return true;
        }
    }
}
