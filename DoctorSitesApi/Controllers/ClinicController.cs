﻿using DoctorSites.DAL;
using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoctorSitesApi.Controllers
{
    public class ClinicController : ApiController
    {
        public bool Post([FromBody]Clinic clinic)
        {
            using (var context = new DoctorDbContext())
            {
                context.Clinics.Add(clinic);
                context.SaveChanges();
            }
            return true;
        }

        public List<ClinicVM> Get()
        {
            List<ClinicVM> clinics = new List<ClinicVM>();
            using (var context = new DoctorDbContext())
            {
                clinics = context.Clinics.Select(g => new ClinicVM { Id = g.Id, Name = g.Name }).ToList();

            }
            return clinics;
        }
    }
}
