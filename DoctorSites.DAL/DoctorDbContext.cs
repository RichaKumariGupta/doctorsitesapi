﻿using DoctorSites.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoctorSites.DAL
{
   public class DoctorDbContext:DbContext
    {
        public DoctorDbContext() : base("name=DefaultConnection")
        {
            Database.SetInitializer<DoctorDbContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<Member> Members { get; set; }
        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Practice> Practices { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Specialist> Specialists { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Entity<Member>().Property(m => m.MobNo).HasColumnName(@"MobileNumber").IsOptional().
            HasColumnType("varchar").HasMaxLength(50);
        }
    }
}
